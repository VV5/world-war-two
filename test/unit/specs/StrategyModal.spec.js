import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import StrategyModal from '@/components/StrategyModal'

const localVue = createLocalVue()
localVue.use(Vuex)

describe('StrategyModal', () => {
  let wrapper
  let store, state

  beforeEach(() => {
    state = {
      selectedStrategy: {
        image: 'hopping-doc.jpg'
      }
    }

    store = new Vuex.Store({
      state
    })

    wrapper = shallowMount(StrategyModal, {
      store, localVue
    })
  })

  test('should close modal when clicking anywhere', () => {
    wrapper.find('.modal-background').trigger('click')

    expect(wrapper.emitted().closeFileModal).toBeTruthy()
  })
})

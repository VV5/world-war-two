import { shallowMount, createLocalVue } from '@vue/test-utils'
import sinon from 'sinon'
import VueRouter from 'vue-router'
import Home from '@/components/Home'

const localVue = createLocalVue()
localVue.use(VueRouter)

describe('Home', () => {
  let wrapper
  let router

  beforeEach(() => {
    router = new VueRouter()

    wrapper = shallowMount(Home, {
      router, localVue
    })

    window.HTMLMediaElement.prototype.load = () => {}
    window.HTMLMediaElement.prototype.play = () => {}
  })

  test('should navigate to introduction correctly when start button is clicked', () => {
    const $route = {
      path: '/introduction'
    }

    const playSoundEffectStub = sinon.stub()

    wrapper.setMethods({ playSoundEffect: playSoundEffectStub })

    wrapper.find('.is-start').trigger('click')

    expect(playSoundEffectStub.called).toBe(true)
    expect(wrapper.vm.$route.path).toBe($route.path)
  })
})

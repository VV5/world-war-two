# Changelog

## v2.0.5
- Fixed compatibility issues with older browsers

## v2.0.4
- Removed `@mdi/font`

## v2.0.3
- Fixed a bug on Strategy component
- Optimised webpack config for production

## v2.0.2
- Fixed background images path not linked properly

## v2.0.1
- Removed e2e testings
- Updated relative paths
- Removed unnecessary `async/await`

## v2.0.0
- Upgraded to Webpack v4

## v1.1.3
- Added a function to play sound effects easily
- Removed background music

## v1.1.2
- Set background music default to muted

## v1.1.1
- Added a button to mute BGM
- Added fade in effect on load

## v1.1.0
- Added background music
- Added sound effects

## v1.0.2
- Added response to report card
- Changed button design for Quiz Instruction page
- Fixed minor text changes
- Fixed an error with button styling not rendering properly on Google Chrome

## v1.0.1
- Added model answers to report page
- Fixed UI layout for Quiz component
- Removed beta version tag

## v1.0.0
### Initial Release

## v1.0.0-beta.2
- Added a function to close modal when `esc` is pressed
- Fixed an issue with quiz not redirecting properly hence allowing users to cheat.
- Fixed styling and positioning of general assets

import { shallowMount, createLocalVue } from '@vue/test-utils'
import sinon from 'sinon'
import VueRouter from 'vue-router'
import SummaryView from '@/components/SummaryView'

const localVue = createLocalVue()
localVue.use(VueRouter)

describe('SummaryView', () => {
  let wrapper
  let router

  beforeEach(() => {
    router = new VueRouter()

    wrapper = shallowMount(SummaryView, {
      router, localVue
    })
  })

  test('should play sound effect when mouse over any buttons', () => {
    const playSoundEffectStub = sinon.stub()
    wrapper.setMethods({ playSoundEffect: playSoundEffectStub })

    wrapper.findAll('button').at(0).trigger('mouseover')

    expect(playSoundEffectStub.called).toBe(true)
  })

  test('should navigate to report when button is clicked', () => {
    const $route = {
      path: '/report'
    }

    const playSoundEffectStub = sinon.stub()
    wrapper.setMethods({ playSoundEffect: playSoundEffectStub })

    wrapper.find('button').trigger('click')

    expect(playSoundEffectStub.called).toBe(true)
    expect(wrapper.vm.$route.path).toBe($route.path)
  })
})

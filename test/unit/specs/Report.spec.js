import { shallowMount, createLocalVue } from '@vue/test-utils'
import sinon from 'sinon'
import Vuex from 'vuex'
import VueRouter from 'vue-router'
import Report from '@/components/Report'

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(VueRouter)

describe('Report', () => {
  let wrapper, store, state
  let router

  beforeEach(() => {
    state = {}

    store = new Vuex.Store({
      state
    })

    router = new VueRouter()

    wrapper = shallowMount(Report, {
      attachToDocument: true,
      store,
      router,
      localVue
    })
  })

  test('should navigate to the start when restart button is clicked', () => {
    const $route = {
      path: '/'
    }

    const playSoundEffectStub = sinon.stub()
    wrapper.setMethods({ playSoundEffect: playSoundEffectStub })

    wrapper.find('.is-restart').trigger('click')

    expect(wrapper.vm.$route.path).toBe($route.path)
  })

  test('should play sound effect when mouse over any buttons', () => {
    const playSoundEffectStub = sinon.stub()
    wrapper.setMethods({ playSoundEffect: playSoundEffectStub })

    wrapper.findAll('button').at(0).trigger('mouseover')

    expect(playSoundEffectStub.called).toBe(true)
  })
})

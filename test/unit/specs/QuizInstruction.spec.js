import { shallowMount, createLocalVue } from '@vue/test-utils'
import sinon from 'sinon'
import VueRouter from 'vue-router'
import QuizInstruction from '@/components/QuizInstruction'

const localVue = createLocalVue()
localVue.use(VueRouter)

describe('QuizInstruction', () => {
  let wrapper
  let router

  beforeEach(() => {
    router = new VueRouter()

    wrapper = shallowMount(QuizInstruction, {
      router, localVue
    })
  })

  test('should play sound effect when mouse over any buttons', () => {
    const playSoundEffectStub = sinon.stub()

    wrapper.setMethods({ playSoundEffect: playSoundEffectStub })

    wrapper.findAll('.is-choice').at(0).trigger('mouseover')

    expect(playSoundEffectStub.called).toBe(true)
  })

  test('should navigate to quiz when button is clicked', () => {
    const $route = {
      path: '/quiz',
      params: { id: 1 }
    }

    const playSoundEffectStub = sinon.stub()

    wrapper.setMethods({ playSoundEffect: playSoundEffectStub })

    wrapper.find('.goto-quiz').trigger('click')

    expect(playSoundEffectStub.called).toBe(true)
    expect(wrapper.vm.$route.path).toBe(`${$route.path}/${$route.params.id}`)
  })

  test('should navigate to strategy when button is clicked', () => {
    const $route = {
      path: '/strategy'
    }

    const playSoundEffectStub = sinon.stub()

    wrapper.setMethods({ playSoundEffect: playSoundEffectStub })

    wrapper.find('.goto-strategy').trigger('click')

    expect(playSoundEffectStub.called).toBe(true)
    expect(wrapper.vm.$route.path).toBe($route.path)
  })
})

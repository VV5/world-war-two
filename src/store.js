import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    version: null,
    quizzes: [
      {
        id: 1,
        question: 'German submarines are sinking our merchant ships in the Atlantic! Without these merchant ships, we would not be able to send food, raw materials and weapons from the USA to our allies, Britain and the Soviet Union. What should we do?',
        image: 'question.png',
        map: 'submarine.png',
        answers: {
          a: {
            text: 'Bomb the German submarines',
            response: 'I do not think so. We do not have enough bombers to patrol the whole ocean. Besides, it would be difficult to find the submarines and hit them with our bombs!',
            image: 'bad.png'
          },
          b: {
            text: 'Send the supplies by air',
            response: 'Nope, that would not work. We do not have any planes that can cross the Atlantic without having to stop in the middle for fuel. Besides, planes cannot carry as much cargo as ships.',
            image: 'bad.png'
          },
          c: {
            text: 'Use the convoy system',
            response: 'Okay, that sounds like a good idea. Why do you think so?',
            image: 'good.png',
            map: 'supply.png'
          },
          d: {
            text: 'Counter with our own submarines',
            response: 'I disagree. We need to protect our own ships, not hunt the enemy submarines.',
            image: 'bad.png'
          }
        },
        correct: {
          option: 'c',
          text: 'Use the convoy system',
          response: 'Here is what I think. Grouping our ships together is our best chance of protecting them from the German submarines. That way, we can protect more ships at the same time and the ships can keep a lookout for each other and for the enemy vessels.'
        },
        selectedAnswers: []
      },
      {
        id: 2,
        question: 'The Japanese are spread out all over the Pacific Ocean and in Asia. How should we attack them?',
        image: 'question.png',
        map: 'ship.png',
        answers: {
          a: {
            text: 'Bomb them',
            response: 'Nah, we do not have any bombers that can fly all over the Pacific and make it back. Is there a better way?',
            image: 'bad.png'
          },
          b: {
            text: 'Attack Japan directly',
            response: 'Nope, that would not work. It is too far to go across the Pacific directly. Our ships and soldiers would need to stop somewhere first. Besides, we cannot just leave enemy strongholds behind our backs while we attack their home country.',
            image: 'bad.png'
          },
          c: {
            text: 'Attack them everywhere at the same time',
            response: 'We do not have the resources to do that, son. Dividing our forces to attack many places at the same time would only make us weaker. Think again!',
            image: 'bad.png'
          },
          d: {
            text: 'Capture one island at a time until we reach Asia',
            response: 'Okay. That might work. Why do you think so?',
            image: 'good.png',
            map: 'ship.png'
          }
        },
        correct: {
          option: 'd',
          text: 'Capture one island at a time until we reach Asia',
          response: 'I see. Island-hopping is a good strategy as it concentrates our forces for stronger assaults on one island at a time, rather than spreading our forces out and making them weaker.'
        },
        selectedAnswers: []
      },
      {
        id: 3,
        question: 'We need to stop the Axis Powers from producing the weapons and vehicles that they can use to attack us. However, our soldiers are too far way to reach their factories. What can we do?',
        image: 'question.png',
        map: 'bombing.png',
        answers: {
          a: {
            text: 'Use spies to sabotage the factories',
            response: 'No, we do not have enough spies and even if we do, they would not be able to do much damage on their own. Try again!',
            image: 'bad.png'
          },
          b: {
            text: 'Shoot missiles at them',
            response: 'Well, I wish we could. But we simply do not have any missiles that can do the job. We need another option.',
            image: 'bad.png'
          },
          c: {
            text: 'Bomb the factories',
            response: 'Sounds like a good plan. Why do you think so?',
            image: 'good.png',
            map: 'bombing.png'
          },
          d: {
            text: 'Do nothing',
            response: 'Do nothing? That is not acceptable, soldier! We cannot admit defeat so easily! Think harder!',
            image: 'bad.png'
          }
        },
        correct: {
          option: 'c',
          text: 'Bomb the factories',
          response: 'Yes, bombing should certainly help. If we could get together enough bombers and hit the biggest German and Japanese cities, we would destroy enough factories and infrastructure to disrupt their war production.'

        },
        selectedAnswers: []
      }
    ],
    strategies: [
      {
        id: 1,
        title: 'Island Hopping',
        text: 'Instead of attacking everywhere at once in the Pacific, attack strategically important islands one by one, establishing a stronghold in each island before moving on to the next. This ensures that our forces are concentrated for maximum impact instead of being spreaded out too thinly.',
        cover: 'hopping-file.png',
        image: 'hopping-doc.jpg'
      },
      {
        id: 2,
        title: 'Convoy',
        text: 'Group ocean-crossing ships together for mutual lookout and protection. Escort them with warships. This reduces the chance of them being sunk by enemy submarines and ensures that vital supplies and reinforcements can reach the battlefront.',
        cover: 'convoy-file.png',
        image: 'convoy-doc.png'
      },
      {
        id: 3,
        title: 'Bombing',
        text: 'Bomb the enemy to destroy their key industries and infrastructure, such as factories, roads and railways. This helps to disrupt their war efforts and reduce the morale of their people.',
        cover: 'bombing-file.png',
        image: 'bombing-doc.png'
      }
    ],
    selectedStrategy: {}
  },
  mutations: {
    setVersion (state, version) {
      state.version = version
    },
    setSelectedStrategy (state, strategy) {
      state.selectedStrategy = strategy
    }
  }
})

import { shallowMount, createLocalVue } from '@vue/test-utils'
import sinon from 'sinon'
import VueRouter from 'vue-router'
import Introduction from '@/components/Introduction'

const localVue = createLocalVue()
localVue.use(VueRouter)

describe('Introduction', () => {
  let wrapper
  let router

  beforeEach(() => {
    router = new VueRouter()

    wrapper = shallowMount(Introduction, {
      router, localVue
    })
  })

  test('should navigate to strategy if button is clicked', () => {
    const $route = {
      path: '/strategy'
    }

    const playSoundEffectStub = sinon.stub()

    wrapper.setMethods({ playSoundEffect: playSoundEffectStub })

    wrapper.find('button').trigger('click')

    expect(playSoundEffectStub.called).toBe(true)
    expect(wrapper.vm.$route.path).toBe($route.path)
  })
})

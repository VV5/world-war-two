import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Introduction from '@/components/Introduction'
import Strategy from '@/components/Strategy'
import StrategyReady from '@/components/StrategyReady'
import QuizInstruction from '@/components/QuizInstruction'
import Quiz from '@/components/Quiz'
import SummaryView from '@/components/SummaryView'
import Report from '@/components/Report'

Vue.use(Router)

export default new Router({
  base: window.location.pathname,
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home,
      meta: {
        title: 'Home'
      }
    },
    {
      path: '/introduction',
      name: 'Introduction',
      component: Introduction,
      meta: {
        title: 'Introduction'
      }
    },
    {
      path: '/strategy',
      name: 'Strategy',
      component: Strategy,
      meta: {
        title: 'Strategies Outline'
      }
    },
    {
      path: '/ready',
      name: 'StrategyReady',
      component: StrategyReady,
      meta: {
        title: 'Strategies Ready'
      }
    },
    {
      path: '/quiz',
      name: 'Instruction',
      component: QuizInstruction,
      meta: {
        title: 'Quiz Instruction'
      }
    },
    {
      path: '/quiz/:id',
      name: 'Quiz',
      component: Quiz,
      meta: {
        title: 'Quiz'
      }
    },
    {
      path: '/summary',
      name: 'Summary',
      component: SummaryView,
      meta: {
        title: 'Summary'
      }
    },
    {
      path: '/report',
      name: 'Report',
      component: Report,
      meta: {
        title: 'Report'
      }
    }
  ]
})

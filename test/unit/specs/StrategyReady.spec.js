import { shallowMount, createLocalVue } from '@vue/test-utils'
import sinon from 'sinon'
import VueRouter from 'vue-router'
import StrategyReady from '@/components/StrategyReady'

const localVue = createLocalVue()
localVue.use(VueRouter)

describe('StrategyReady', () => {
  let wrapper
  let router

  beforeEach(() => {
    router = new VueRouter()

    wrapper = shallowMount(StrategyReady, {
      router, localVue
    })
  })

  test('should navigate to quiz correctly when button is clicked', () => {
    const $route = {
      path: '/quiz'
    }

    const playSoundEffectStub = sinon.stub()
    wrapper.setMethods({ playSoundEffect: playSoundEffectStub })

    wrapper.find('button').trigger('click')

    expect(playSoundEffectStub.called).toBe(true)
    expect(wrapper.vm.$route.path).toBe($route.path)
  })
})
